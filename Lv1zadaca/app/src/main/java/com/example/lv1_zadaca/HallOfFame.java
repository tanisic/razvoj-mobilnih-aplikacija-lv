package com.example.lv1_zadaca;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class HallOfFame {
    private String name;
    private String about;
    private String famousClub;
    private Drawable photo;



    public String getName() {
        return name;
    }

    public String getAbout() {
        return about;
    }

    public String getFamousClub() {
        return famousClub;
    }

    public Drawable getPhoto() {
        return photo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public void setFamousClub(String famousClub) {
        this.famousClub = famousClub;
    }

    public void setPhoto(Drawable photo) {
        this.photo = photo;
    }
}
