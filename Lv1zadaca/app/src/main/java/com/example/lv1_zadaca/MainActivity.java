package com.example.lv1_zadaca;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView ivPlayer1,ivPlayer2,ivPlayer3;
    private TextView tvPlayerName1,tvPlayerAbout1,tvPlayerName2,tvPlayerAbout2,
            tvPlayerName3,tvPlayerAbout3;
    private HallOfFame drazen, oscar, stipe;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeUI();
    }
    private void initializeUI(){
        ivPlayer1 = findViewById(R.id.ivPlayerImage1);
        tvPlayerName1 = findViewById(R.id.tvPlayerName1);
        tvPlayerAbout1 = findViewById(R.id.tvPlayerAbout1);
        drazen = new HallOfFame();
        drazen.setName("Dražen Petrović");
        drazen.setFamousClub(getApplicationContext().getString(R.string.drazenToast));
        drazen.setPhoto(getApplicationContext().getDrawable(R.drawable.drazen));
        drazen.setAbout(getApplicationContext().getString(R.string.about_drazen));
        ivPlayer1.setImageDrawable(drazen.getPhoto());
        tvPlayerName1.setText(drazen.getName());
        tvPlayerAbout1.setText(drazen.getAbout());

        ivPlayer2 = findViewById(R.id.ivPlayerImage2);
        tvPlayerName2 = findViewById(R.id.tvPlayerName2);
        tvPlayerAbout2 = findViewById(R.id.tvPlayerAbout2);
        oscar = new HallOfFame();
        oscar.setName("Oscar Robertson");
        oscar.setFamousClub(getApplicationContext().getString(R.string.oscarToast));
        oscar.setPhoto(getApplicationContext().getDrawable(R.drawable.oscar));
        oscar.setAbout(getApplicationContext().getString(R.string.about_oscar));
        ivPlayer2.setImageDrawable(oscar.getPhoto());
        tvPlayerName2.setText(oscar.getName());
        tvPlayerAbout2.setText(oscar.getAbout());

        ivPlayer3 = findViewById(R.id.ivPlayerImage3);
        tvPlayerName3 = findViewById(R.id.tvPlayerName3);
        tvPlayerAbout3 = findViewById(R.id.tvPlayerAbout3);
        stipe = new HallOfFame();
        stipe.setName("Stipe Miocic");
        stipe.setFamousClub(getApplicationContext().getString(R.string.stipeToast));
        stipe.setPhoto(getApplicationContext().getDrawable(R.drawable.stipe));
        stipe.setAbout(getApplicationContext().getString(R.string.about_stipe));
        ivPlayer3.setImageDrawable(stipe.getPhoto());
        tvPlayerName3.setText(stipe.getName());
        tvPlayerAbout3.setText(stipe.getAbout());


        ivPlayer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Toast toast = new Toast(MainActivity.this);
                    toast.setText(drazen.getFamousClub());
                    toast.show();
                }catch (Exception e){}

            }
        });
        ivPlayer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast toast = new Toast(MainActivity.this);
                    toast.setText(oscar.getFamousClub());
                    toast.show();
                }catch (Exception e){}
            }
        });
        ivPlayer3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Toast toast = new Toast(MainActivity.this);
                    toast.setText(stipe.getFamousClub());
                    toast.show();
                }catch (Exception e){}
            }
        });

    }

}