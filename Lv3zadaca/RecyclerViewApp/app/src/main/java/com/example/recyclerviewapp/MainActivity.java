package com.example.recyclerviewapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements NameClickListener{
    private static String TAG = "MainActivity";
    private RecyclerView recyclerView;
    private List<String> dataList;
    private CustomAdapter customAdapter;
    private Button addButton;
    private EditText addEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupData();
        setupAddView();
        setupRecyclerView();

    }
    private void setupData(){
        dataList = new ArrayList<>();
        dataList.add("Anna");
        dataList.add("Milovan");
        dataList.add("Marko");
        dataList.add("Janko");
        dataList.add("Petar");
        dataList.add("Ivona");
        dataList.add("Marija");
    }

    private void setupRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        customAdapter = new CustomAdapter(dataList,this);
        recyclerView.setAdapter(customAdapter);
    }

    private void setupAddView(){
        addButton = findViewById(R.id.addButton);
        addEditText = findViewById(R.id.addEditText);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToList();
            }
        });
    }

    private void addToList(){
        Collections.reverse(dataList);
        dataList.add(addEditText.getText().toString());
        Collections.reverse(dataList);
        customAdapter.notifyDataSetChanged();
    }



    @Override
    public void onDeleteClick(int position) {
        dataList.remove(position);
        customAdapter.notifyDataSetChanged();
        Log.d(TAG, "onNameClick MainActivtiy "+position);
    }

    @Override
    public void onTextClick(int position) {
        Intent intent = new Intent(this, StudentDetailActivity.class);
        intent.putExtra(StudentDetailFragment.ARG_ITEM_ID, dataList.get(position));
        startActivity(intent);
    }


}