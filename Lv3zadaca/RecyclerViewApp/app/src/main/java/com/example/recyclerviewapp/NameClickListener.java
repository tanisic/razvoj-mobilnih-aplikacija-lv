package com.example.recyclerviewapp;

public interface NameClickListener {
    void onDeleteClick(int position);
    void onTextClick(int position);
}
